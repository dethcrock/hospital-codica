Rails.application.routes.draw do
  devise_for :doctors
  #resources :discharges 
  get 'discharges/:id/show', to: 'discharges#show', as: 'discharge'
  post 'record/:id/discharge/new', to:'discharges#create', as: 'new_record_discharge'
  get 'record/:id/discharge/new', to: 'discharges#new'
  
  resources :doctor
  resources :records
  post 'doctor/:id/records/new', to: 'records#create', as: 'new_doctor_record'
  get 'doctor/:id/records/new', to: 'records#new'
  get 'doctor/:id/records/index', to: 'records#index',
        as: 'doctor_records'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, controllers: {sessions: 'users/sessions'}
  root 'main#home'
  get 'user/profile', as: 'user_root'
  get 'doctor/profile', as: 'doctor_root'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
