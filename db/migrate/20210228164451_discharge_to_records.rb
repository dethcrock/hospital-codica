class DischargeToRecords < ActiveRecord::Migration[6.1]
  def change
    change_table :discharges do |t|
      t.belongs_to :record
    end
  end
end
