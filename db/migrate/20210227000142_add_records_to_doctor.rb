class AddRecordsToDoctor < ActiveRecord::Migration[6.1]
  def change
      change_table :records do |t|
          t.belongs_to :doctor
      end
  end
end
