class AddRecordsToUser < ActiveRecord::Migration[6.1]
  def change
      change_table :records do |t|
          t.belongs_to :user
      end
  end
end
