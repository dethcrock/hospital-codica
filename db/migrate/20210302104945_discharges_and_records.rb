class DischargesAndRecords < ActiveRecord::Migration[6.1]
  def change
    change_table :records do |t|
      t.boolean :isOpen
    end

  end
end
