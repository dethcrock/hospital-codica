class AddDoctorsToCategory < ActiveRecord::Migration[6.1]
  def change
    change_table :doctors do |t|
      t.belongs_to :category
    end
  end
end
