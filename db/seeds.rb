# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@codica.com', password: 'codicaqwerty', password_confirmation: 'codicaqwerty') if Rails.env.development?

Doctor.create!(name: 'Виктория', surname: 'Смирнова', phone: '669551763', password: 'doctorcodica', password_confirmation: 'doctorcodica') if Rails.env.development? 
User.create!(phone: '986209719', password: '28d10f31', password_confirmation: '28d10f31', name: 'Nikita', surname: 'Whales')
