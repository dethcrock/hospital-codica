ActiveAdmin.register Doctor do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
   #permit_params :name, :surname
  
   #or
 
   permit_params do
   	permitted = [:name, :surname, :phone, :password,
                  :category_id]
   	permitted << :other if params[:action] == 'create' && current_admin_user
  	 permitted
   end

  
   form title: 'New doctor' do|f|
     inputs do
       panel 'Account data' do
         input :phone
         input :password
         #input :password_confirmation
       end
       panel 'Doctor info' do
         input :name
         input :surname
       end
       panel 'Category' do
         f.collection_select :category_id,
           Category.all, :id, :title
       end
       actions
     end
   end
end
