class RecordsController < InheritedResources::Base
  before_action :set_record, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, only: [:create]

  # GET /records or /records.json
  def index
    @records = Record.all.where(doctor_id: params[:id])
  end

  # GET /records/1 or /records/1.json
  def show
      @record = Record.find(params[:id])
  end

  # GET /records/new
  def new
    @record = Record.new
  end

  # GET /records/1/edit
  def edit
  end

  def doctor
    @doctor = Doctor.find(@record.id)
  end

  # POST /records or /records.json
  def create
    @record = Record.new(record_params)
    @record.user = current_user
    @record.doctor_id = params[:id]
    @record.isOpen = true

    @count = Record.where(doctor_id: params[:id]).count
    respond_to do |format|
    @count += 1
    if @count <= 10
      if @record.save
        format.html { redirect_to user_root_path, notice: "Запись успешно создана."}
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    else
	format.html{ redirect_to doctor_index_path, notice: "Извините но у врача максимально допустимое количество записей"}
    end
  end
  end

  # PATCH/PUT /records/1 or /records/1.json
  def update
    respond_to do |format|
      if @record.update(record_params)
        format.html { redirect_to @record, notice: "Record was successfully updated." }
        format.json { render :show, status: :ok, location: @record }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /records/1 or /records/1.json
  def destroy
    @record.destroy
    respond_to do |format|
      format.html { redirect_to records_url, notice: "Record was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_record
      @record = Record.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def record_params
        params.
	   permit(:title, :description, :record_at)
    end
end
