class DischargesController < InheritedResources::Base

  def index
    @discharges = Discharge.all
  end

  # GET /discharges/1 or /records/1.json
  def show
    unless Discharge.where(record_id: params[:id]).exists?
        #render template: 'discharges/_notexists'
        flash.notice = 'Извините но доктор еще не дал свою рекомендацию'
        redirect_back  fallback_location: { action: "profile"}
      else
        @discharge = Discharge.where(record_id: params[:id]).first
      end
  end

  # GET /discharges/new
  def new
    @discharge = Discharge.new
  end

  # GET /discharges/1/edit
  def edit
  end

  # POST /discharges or /records.json
  def create
    @discharge = Discharge.new(discharge_params)
    @discharge.record_id = params[:id]
    @record = Record.find(@discharge.record_id)
    @record.isOpen = false
    @record.save

    respond_to do |format|
      if @discharge.save
        format.html { redirect_to root_path, notice: "Discharge was successfully created." }
        redirect_to current_doctor
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @discharge.errors, status: :unprocessable_entity }
      end
    else
	format.html{ redirect_to doctor_index_path, notice: "Sorry to many discharges"}
    end
  end

  # PATCH/PUT /discharges/1 or /records/1.json
  def update
    respond_to do |format|
      if @discharge.update(record_params)
        format.html { redirect_to @discharge, notice: "Discharge was successfully updated." }
        format.json { render :show, status: :ok, location: @discharge }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @discharge.errors, status: :unprocessable_entity }
      end
    end
  end
  private

    def discharge_params
      params.permit(:body)
    end

end
