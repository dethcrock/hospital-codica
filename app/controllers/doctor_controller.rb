class DoctorController < ApplicationController
    
    def index
      if doctor_signed_in?
        @doctors = Doctor.where.
            not(id: current_doctor.id)
      else
    	  @doctors = Doctor.all
      end
    end

    def show
      @doctor = current_doctor
    end
end
