class ApplicationController < ActionController::Base
  before_action :configure_permitted_params, if: :devise_controller?

    protected
    def configure_permitted_params
      attributes = [:name, :surname, :phone, :password]
      devise_parameter_sanitizer.permit(:sign_up, keys: attributes)
      devise_parameter_sanitizer.permit(:account_update, keys: attributes)
      devise_parameter_sanitizer.permit(:sign_in, keys: [:phone, :password])

    end
end
