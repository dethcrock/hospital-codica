json.extract! record, :id, :title, :description, :record_at, :created_at, :updated_at
json.url record_url(record, format: :json)
