json.extract! discharge, :id, :created_at, :updated_at
json.url discharge_url(discharge, format: :json)
